#
# DeploySysinternalsSuite:  This script, developed for Windows computers,
# allows administrators to deploy all of the diagnostic tools within the
# Sysinternals Suite ZIP file, all at once.
#
# This script creates a well-organized Program Files folder containing all of
# the tools in the Suite, a Start Menu folder with links to launch each of the
# interactive tools, and a "Sysinternals Console" link which opens up a Command
# Prompt window, and pre-loads directories containing the Sysinternals console
# tools from the Suite into the %PATH% environment variable.
#
#
# NOTES:
#
# This script depends upon an external copy of the Sysinternals Suite ZIP file
# ("SysinternalsSuite.zip").  This file can be named anything the user likes,
# as long as it remains a ZIP file ("*.zip") and it's in or underneath the
# folder where this script is located (so the script can find it at run time).
#
# This script does NOT self-contain or redistribute that file, as that would
# violate the copyright license from the Microsoft Windows Sysinternals team.
#
# (As of this writing, the user can easily obtain that file on his/her own from
#  "https://docs.microsoft.com/en-us/sysinternals/downloads/sysinternals-suite",
#  or by opening a search engine and searching for "Sysinternals suite".)
#
#
# COPYRIGHT:
#
# Copyright (C) July, 2020  JackOfAll_IT_Trades
# (JackOfAll.IT.Trades@gmail.com)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License (LGPL) as published by the
# Free Software Foundation, version 3 (LGPLv3).
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License, version
# 3 for more details.
#
# You should have received a copy of the GNU Lesser General Public License,
# version 3, along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

Write-Host ("
DeploySysinternalsSuite:  A local deployment script for the Sysinternals Suite.
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License (LGPL) as published by the
Free Software Foundation, version 3 (LGPLv3).  For more details, read the code.
")

$ProgFiles_SuiteDirectory = "\Sysinternals Suite"
$StartMenu_SuiteFolder    = $ProgFiles_SuiteDirectory

# Before we begin, initialize the mappings that will define where we put each file in the suite:
#
function InitializeFileMapping
{
	$suiteFileMapping = @{}
	# Example:
	#
	# $suiteFileMapping["newFileName.exe"]   = @{ "ProgFiles_RelPath" = "<NULL>";                                         "StartMenu_LinkName" = "<NULL>";                        "StartMenu_RelPath" = "<NULL>";                 }
	#
	$suiteFileMapping["ADExplorer.exe"]      = @{ "ProgFiles_RelPath" = "\Active Directory & NetBIOS Tools\AD Explorer\"; "StartMenu_LinkName" = "AD Explorer.LNK";               "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["ADInsight.chm"]       = @{ "ProgFiles_RelPath" = "\Active Directory & NetBIOS Tools\AD Insight\";  "StartMenu_LinkName" = "AD Insight Help.LNK";           "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["ADInsight.exe"]       = @{ "ProgFiles_RelPath" = "\Active Directory & NetBIOS Tools\AD Insight\";  "StartMenu_LinkName" = "AD Insight.LNK";                "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["AccessEnum.exe"]      = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\";                             "StartMenu_LinkName" = "AccessEnum.LNK";                "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["AdExplorer.chm"]      = @{ "ProgFiles_RelPath" = "\Active Directory & NetBIOS Tools\AD Explorer\"; "StartMenu_LinkName" = "AD Explorer Help.LNK";          "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Autologon.exe"]       = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\";                          "StartMenu_LinkName" = "Autologon.LNK";                 "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["autoruns.exe"]        = @{ "ProgFiles_RelPath" = "\Startup Troubleshooting Tools\";                "StartMenu_LinkName" = "Autoruns.LNK";                  "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Autoruns64.exe"]      = @{ "ProgFiles_RelPath" = "\Startup Troubleshooting Tools\";                "StartMenu_LinkName" = "Autoruns64.LNK";                "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["autoruns.chm"]        = @{ "ProgFiles_RelPath" = "\Startup Troubleshooting Tools\";                "StartMenu_LinkName" = "Autoruns Help.LNK";             "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Autoruns64.dll"]      = @{ "ProgFiles_RelPath" = "\Startup Troubleshooting Tools\";                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Autoruns64a.dll"]     = @{ "ProgFiles_RelPath" = "\Startup Troubleshooting Tools\";                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["autorunsc.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["autorunsc64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Bginfo.exe"]          = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\";                          "StartMenu_LinkName" = "BgInfo.LNK";                    "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["Bginfo.exe"]          = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\";                          "StartMenu_LinkName" = "BgInfo.LNK";                    "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["Bginfo64.exe"]        = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\";                          "StartMenu_LinkName" = "Bginfo64.LNK";                  "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["Cacheset.exe"]        = @{ "ProgFiles_RelPath" = "\Performance Monitoring & Tuning\";              "StartMenu_LinkName" = "CacheSet.LNK";                  "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Clockres.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Clockres64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Contig.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Contig64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Coreinfo.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Coreinfo64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["CPUSTRES.EXE"]        = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\CPUSTRES\";                 "StartMenu_LinkName" = "CPU Stress [CPUSTRES].LNK";     "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["CPUSTRES64.EXE"]      = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\CPUSTRES\";                 "StartMenu_LinkName" = "CPU Stress (64-bit).LNK";       "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["DISKMON.HLP"]         = @{ "ProgFiles_RelPath" = "\Performance Monitoring & Tuning\DiskMon\";      "StartMenu_LinkName" = "DiskMon Help.LNK";              "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["DMON.SYS"]            = @{ "ProgFiles_RelPath" = "\Performance Monitoring & Tuning\DiskMon\";      "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Dbgview.exe"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\DebugView\";          "StartMenu_LinkName" = "DebugView.LNK";                 "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Desktops.exe"]        = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\";                          "StartMenu_LinkName" = "Desktops.LNK";                  "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["Disk2vhd.chm"]        = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\Disk2vhd\";                 "StartMenu_LinkName" = "Disk2vhd Help.LNK";             "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["DiskView.exe"]        = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\";                             "StartMenu_LinkName" = "DiskView.LNK";                  "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["Diskmon.exe"]         = @{ "ProgFiles_RelPath" = "\Performance Monitoring & Tuning\DiskMon\";      "StartMenu_LinkName" = "DiskMon.LNK";                   "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Eula.txt"]            = @{ "ProgFiles_RelPath" = "\";                                              "StartMenu_LinkName" = "Sysinternals Suite EULA.LNK";   "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["FindLinks.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["FindLinks64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Listdlls.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Listdlls64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["LoadOrd.exe"]         = @{ "ProgFiles_RelPath" = "\Startup Troubleshooting Tools\";                "StartMenu_LinkName" = "LoadOrder.LNK";                 "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["LoadOrd64.exe"]       = @{ "ProgFiles_RelPath" = "\Startup Troubleshooting Tools\";                "StartMenu_LinkName" = "LoadOrder64.LNK";               "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["LoadOrdC.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["LoadOrdC64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PORTMON.CNT"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Portmon\";            "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["PORTMON.HLP"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Portmon\";            "StartMenu_LinkName" = "Portmon Help.LNK";              "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Procmon.exe"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Process Monitor\";    "StartMenu_LinkName" = "Process Monitor.LNK";           "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Procmon64.exe"]       = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Process Monitor\";    "StartMenu_LinkName" = "Process Monitor (64-bit).LNK";  "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["PsExec.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsExec64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsGetsid.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsGetsid64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsInfo.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsInfo64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsLoggedon.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsLoggedon64.exe"]    = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsService.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["PsService64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Pstools.chm"]         = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "PsTools Help.LNK";              "StartMenu_RelPath" = "\Sysinternals Console\"; }
	$suiteFileMapping["RAMMap.exe"]          = @{ "ProgFiles_RelPath" = "\Performance Monitoring & Tuning\";              "StartMenu_LinkName" = "RAMMap.LNK";                    "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["RegDelNULL.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["RegDelNull64.exe"]    = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
#	$suiteFileMapping["RootkitRevealer.chm"] = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\Rootkit Revealer\";            "StartMenu_LinkName" = "Rootkit Revealer Help.LNK";     "StartMenu_RelPath" = "<SAME>";                 }
#	$suiteFileMapping["RootkitRevealer.exe"] = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\Rootkit Revealer\";            "StartMenu_LinkName" = "Rootkit Revealer.LNK";          "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["ShareEnum.exe"]       = @{ "ProgFiles_RelPath" = "\Active Directory & NetBIOS Tools\";             "StartMenu_LinkName" = "ShareEnum.LNK";                 "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["ShellRunas.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Sysmon.exe"]          = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\";                    "StartMenu_LinkName" = "Sysmon.LNK";                    "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Sysmon64.exe"]        = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\";                    "StartMenu_LinkName" = "Sysmon64.LNK";                  "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["TCPVIEW.HLP"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\TCPView\";            "StartMenu_LinkName" = "TCPView Help (HLP).LNK";        "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Tcpvcon.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Tcpview.exe"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\TCPView\";            "StartMenu_LinkName" = "TCPView.LNK";                   "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Testlimit.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Testlimit64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Vmmap.chm"]           = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\VMMap\";                       "StartMenu_LinkName" = "VMMap Help.LNK";                "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Volumeid.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["Volumeid64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["WINOBJ.HLP"]          = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\WinObj\";                      "StartMenu_LinkName" = "WinObj Help.LNK";               "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["Winobj.exe"]          = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\WinObj\";                      "StartMenu_LinkName" = "WinObj.LNK";                    "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["ZoomIt.exe"]          = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\";                          "StartMenu_LinkName" = "ZoomIt.LNK";                    "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["accesschk.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["accesschk64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["adrestore.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ctrl2cap.amd.sys"]    = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\Ctrl2cap\";                 "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ctrl2cap.exe"]        = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\Ctrl2cap\";                 "StartMenu_LinkName" = "Ctrl2cap.LNK";                  "StartMenu_RelPath" = "\Miscellaneous Tools\";  }
	$suiteFileMapping["ctrl2cap.nt4.sys"]    = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\Ctrl2cap\";                 "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ctrl2cap.nt5.sys"]    = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\Ctrl2cap\";                 "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["dbgview.chm"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\DebugView\";          "StartMenu_LinkName" = "DebugView Help.LNK";            "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["disk2vhd.exe"]        = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\Disk2vhd\";                 "StartMenu_LinkName" = "Disk2vhd.LNK";                  "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["diskext.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["diskext64.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["du.exe"]              = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["du64.exe"]            = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["efsdump.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["handle.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["handle64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["hex2dec.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["hex2dec64.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["junction.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["junction64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ldmdump.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["livekd.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }                            
	$suiteFileMapping["livekd64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }                            
	$suiteFileMapping["logonsessions.exe"]   = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["logonsessions64.exe"] = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["movefile.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["movefile64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["notmyfault.exe"]      = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\NotMyFault\";               "StartMenu_LinkName" = "NotMyFault.LNK";                "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["notmyfault64.exe"]    = @{ "ProgFiles_RelPath" = "\Miscellaneous Tools\NotMyFault\";               "StartMenu_LinkName" = "NotMyFault64.LNK";              "StartMenu_RelPath" = "<SAME>";                 }                            
	$suiteFileMapping["notmyfaultc.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["notmyfaultc64.exe"]   = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ntfsinfo.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ntfsinfo64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pagedfrg.exe"]        = @{ "ProgFiles_RelPath" = "\Performance Monitoring & Tuning\PageDefrag\";   "StartMenu_LinkName" = "PageDefrag.LNK";                "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["pagedfrg.hlp"]        = @{ "ProgFiles_RelPath" = "\Performance Monitoring & Tuning\PageDefrag\";   "StartMenu_LinkName" = "PageDefrag Help.LNK";           "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["pendmoves.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pendmoves64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pipelist.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pipelist64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["portmon.exe"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Portmon\";            "StartMenu_LinkName" = "Portmon.LNK";                   "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["procdump.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["procdump64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["procexp.chm"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Process Explorer\";   "StartMenu_LinkName" = "Process Explorer Help.LNK";     "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["procexp.exe"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Process Explorer\";   "StartMenu_LinkName" = "Process Explorer.LNK";          "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["procexp64.exe"]       = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Process Explorer\";   "StartMenu_LinkName" = "Process Explorer 64.LNK";       "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["procmon.chm"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\Process Monitor\";    "StartMenu_LinkName" = "Process Monitor Help.LNK";      "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["psfile.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["psfile64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pskill.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pskill64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pslist.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pslist64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["psloglist.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["psloglist64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pspasswd.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pspasswd64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["psping.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["psping64.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["psshutdown.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pssuspend.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["pssuspend64.exe"]     = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["psversion.txt"]       = @{ "ProgFiles_RelPath" = "\Console Tools\PsTools\";                        "StartMenu_LinkName" = "PsTools Version Info.LNK";      "StartMenu_RelPath" = "\Sysinternals Console\"; }
	$suiteFileMapping["readme.txt"]          = @{ "ProgFiles_RelPath" = "\";                                              "StartMenu_LinkName" = "Sysinternals Suite Readme.LNK"; "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["regjump.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ru.exe"]              = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["ru64.exe"]            = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["sdelete.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["sdelete64.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["sigcheck.exe"]        = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["sigcheck64.exe"]      = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["streams.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["streams64.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["strings.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["strings64.exe"]       = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["sync.exe"]            = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["sync64.exe"]          = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["tcpview.chm"]         = @{ "ProgFiles_RelPath" = "\Activity Monitoring Tools\TCPView\";            "StartMenu_LinkName" = "TCPView Help (CHM).LNK";        "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["vmmap.exe"]           = @{ "ProgFiles_RelPath" = "\Diagnostic Tools\VMMap\";                       "StartMenu_LinkName" = "VMMap.LNK";                     "StartMenu_RelPath" = "<SAME>";                 }
	$suiteFileMapping["whois.exe"]           = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }
	$suiteFileMapping["whois64.exe"]         = @{ "ProgFiles_RelPath" = "\Console Tools\";                                "StartMenu_LinkName" = "<NONE>";                        "StartMenu_RelPath" = "<NONE>";                 }

	foreach ($key in $suiteFileMapping.keys)
	{
		$suiteFileMapping[$key]["bFoundInZIP"] = $false
	}
	return $suiteFileMapping
}

$sysinternalsFileMapping = InitializeFileMapping


# The following is a useful Pause() utility function, shamelessly stolen from
# "https://stackoverflow.com/questions/20886243/press-any-key-to-continue":
#
Function Pause ($message)
{
    # Check if running Powershell ISE
    if ($psISE)
    {
        Add-Type -AssemblyName System.Windows.Forms
        [System.Windows.Forms.MessageBox]::Show("$message")
    }
    else
    {
        Write-Host "$message" -ForegroundColor Yellow
        $x = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    }
}


# The following Administrator-related code is shamelessly stolen from Keith Hill's awesome example 
# at "http://rkeithhill.wordpress.com/2013/04/05/powershell-script-that-relaunches-as-admin/":
#
function IsAdministrator
{
	$Identity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
	$Principal = New-Object System.Security.Principal.WindowsPrincipal($Identity)
	$Principal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
}

function IsUacEnabled
{
	(Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System).EnableLua -ne 0
}


# Administrator verification section (prelude to main script) :
#
if (!(IsAdministrator))
{
	if (IsUacEnabled)
	{
		[string[]]$argList = @('-NoProfile', '-NoExit', '-File', '"' + $MyInvocation.MyCommand.Path + '"')
		$argList += $MyInvocation.BoundParameters.GetEnumerator() | Foreach {"-$($_.Key)", "$($_.Value)"}
		$argList += $MyInvocation.UnboundArguments
		Start-Process PowerShell.exe -Verb Runas -WorkingDirectory $pwd -ArgumentList $argList 
		return
	}
	else
	{
		$errorMessage = "ERROR:  You must be administrator to run this script!"
		Write-Host $errorMessage
		Write-Host "Press any key to continue..."
		$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
		throw $errorMessage
	}
}
#
# End of Administrator verification.  Main script begins below.


# First, compute the path to our script, and collect the list of nearby ZIP files:
#
Write-Host ("Looking for path to DeploySysinternalsSuite script...")
$scriptPath  = split-path -parent $MyInvocation.MyCommand.Definition

Write-Host ("Script path found.  Computing list of all nearby ZIP files...")
$zipFileList = Get-ChildItem -Path $scriptPath -Recurse �File *.zip


# Next, search through those ZIP files to figure out which one is the most recent:
#
Write-Host ("ZIP files found.  Searching through those ZIP files to identify the most recent one...")
$mostRecentZipFileFound = @{}
$mostRecentZipFileFound["LastWriteTime"] = 0
$mostRecentZipFileFound["FilePath"]      = "<None>"

$currentFileIndex = 0
while ($currentFileIndex -lt $zipFileList.count)
{
	$currentFile = $zipFileList[$currentFileIndex]
	if ($currentFile.LastWriteTime -gt $mostRecentZipFileFound["LastWriteTime"])
	{
		$mostRecentZipFileFound["LastWriteTime"] = $currentFile.LastWriteTime
		$mostRecentZipFileFound["FilePath"]      = $currentFile.FullName
	}
	$currentFileIndex++
}


# Then, compute the directory (in "Program Files") where we will place the tool suite,
# and create it if necessary:
#
Write-Host ("Computing destination directory (in 'Program Files') folder...")
$progFilesDirectory_fullPath = [io.path]::GetFullPath($env:ProgramFiles) + $ProgFiles_SuiteDirectory
if (!(Test-Path -Path $progFilesDirectory_fullPath -PathType Container)) {
	Write-Host ("Program Files directory [" + $progFilesDirectory_fullPath + "] does not exist.  Creating...")
	New-Item -ItemType directory -Path $progFilesDirectory_fullPath > $null
}


# Next, do the same for the Start Menu folder:
#
Write-Host ("Computing Start Menu folder...")
$allUsers_StartMenuPath = [io.path]::GetFullPath($env:AllUsersProfile) + "\Microsoft\Windows\Start Menu\Programs"
$startMenuFolder_fullPath = $allUsers_StartMenuPath + $StartMenu_SuiteFolder

if (!(Test-Path -Path $startMenuFolder_fullPath -PathType Container)) {
	Write-Host ("Start Menu folder [" + $startMenuFolder_fullPath + "] does not exist.  Creating...")
	New-Item -ItemType directory -Path $startMenuFolder_fullPath > $null
}


# Initialize console and PsTools paths, and Windows shell and WScript shell objects:
#
$consoleTools_binPath = $progFilesDirectory_fullPath + "\Console Tools"
$psTools_binPath      = $consoleTools_binPath        + "\PsTools"

$shellObject = new-object -com shell.application
$wshShellObj = new-object -comObject WScript.Shell


# Then, attempt to open the ZIP file, and iterate through its contents:
#
$zipFileObj  = $shellObject.NameSpace($mostRecentZipFileFound["FilePath"])
if (!$zipFileObj)
{
	Write-Host ("ERROR:  Unable to open ZIP file [" + $mostRecentZipFileFound["FilePath"] + "].")
	Write-Host ("Exiting.")
	return
}
Write-Host ("Opened zipFileObj: [" + $mostRecentZipFileFound["FilePath"] + "]")

$containedFiles = $zipFileObj.items()
Write-Host ("(Found [" +  $containedFiles.count + "] items.)")

foreach( $item in $zipFileObj.items() )
{
	Write-Host -NoNewLine ("Found item: [" + $item.name + "]...  ")
	$currentMapping = $sysinternalsFileMapping[$item.name]
	if (!$currentMapping)
	{
		Write-Host ("")
		Write-Host ("WARNING:  Unable to find destination mapping for file [" + $item.name + "].  Skipping...")
		continue
	}

	# Okay, we found a mapping for this file.  Mark the array appropriately for our
	# records, and read the mapping to see where this file is supposed to go:
	#
	$sysinternalsFileMapping[$item.name]["bFoundInZIP"] = $true

	#	$itemDestination_directoryPath = '"' + $progFilesDirectory_fullPath + $currentMapping["ProgFiles_RelPath"] + '"'
	$itemDestination_directoryPath = $progFilesDirectory_fullPath + $currentMapping["ProgFiles_RelPath"]
	if (!(Test-Path -Path $itemDestination_directoryPath -PathType Container)) {
#		Write-Host ("Destination directory [" + $itemDestination_directoryPath + "] does not exist.  Creating...")
		New-Item -ItemType directory -Path $itemDestination_directoryPath > $null
	}

	# Compute the full destination path for the file, and attempt to extract it:
	#
	$itemDestination_fullFilePath = $itemDestination_directoryPath + $item.name

#	Write-Host ("Attempting to extract item [" + $item.name + "] to location [" + $itemDestination_fullFilePath + "]")
	if (Test-Path -Path $itemDestination_fullFilePath -PathType Leaf)
	{
		# File already exists.  Check if the file on disk is older than our ZIP file item:
		#
#		if ($itemDestination_fullFilePath.LastWriteTime -lt $mostRecentZipFileFound["LastWriteTime"])
		$fileOnDisk = (Get-Item $itemDestination_fullFilePath)

		if ($fileOnDisk.LastWriteTime -lt $item.ModifyDate)
		{
			# File on disk is older than our ZIP file item.  Delete it:
			#
			Write-Host -NoNewLine ("Deleting existing file...  ")
			Remove-Item $itemDestination_fullFilePath
			# And now the old file is gone.  Extract away!  =)
			#
			Write-Host -NoNewLine ("Extracting new file...  ")
			$shellObject.Namespace($itemDestination_directoryPath).copyhere($item)
		}
		else
		{
			Write-Host -NoNewLine ("Existing file is newer than ZIP item; won't replace.  ")
		}
	}
	else
	{
		# File does not exist.  Extract away!  =)
		#
		Write-Host -NoNewLine ("Extracting file...  ")
		$shellObject.Namespace($itemDestination_directoryPath).copyhere($item)
	}

	# Next, check if we want to create a Start Menu shortcut for the current item:
	#
	if ($currentMapping["StartMenu_LinkName"] -ne "<NONE>")
	{
		# Compute the destination folder for our Start Menu shortcut, and create it if necessary:
		#
		$currentShortcut_relPath     = $currentMapping["StartMenu_RelPath"]
		if ($currentShortcut_relPath -eq "<SAME>") {
			$currentShortcut_relPath = $currentMapping["ProgFiles_RelPath"]
		}
		$startMenuShortcut_directoryPath = $startMenuFolder_fullPath + $currentShortcut_relPath
		if (!(Test-Path -Path $startMenuShortcut_directoryPath -PathType Container)) {
#			Write-Host ("Start Menu folder [" + $startMenuShortcut_directoryPath + "] does not exist.  Creating...")
			New-Item -ItemType directory -Path $startMenuShortcut_directoryPath > $null
		}

		# And now, create the shortcut (link file) for the Start Menu:
		#
		$startMenuShortcut_fullFilePath = $startMenuShortcut_directoryPath + $currentMapping["StartMenu_LinkName"]
#		Write-Host ("Attempting to create Start Menu shortcut [" + $startMenuShortcut_fullFilePath + "]...")

		Write-Host -NoNewLine ("Creating Start Menu shortcut...  ")
		$objShortCut = $wshShellObj.CreateShortcut( $startMenuShortcut_fullFilePath )
		$objShortCut.TargetPath = $itemDestination_fullFilePath
		$objShortCut.Save()
	}

	#	Write-Host ("Done with [" + $item.name + "].  Continuing to next item in ZIP file...")
	Write-Host ("Done.")
}
Write-Host ("Done iterating over items in ZIP file.")


Write-Host ("Verifying that all expected files were found...")
$bAllFilesFound = $true
foreach ($key in $sysinternalsFileMapping.keys)
{
	$currentMapping   = $sysinternalsFileMapping[$key]
	$currentFileFound = $currentMapping["bFoundInZIP"]
	if (!$currentFileFound)
	{
		$bAllFilesFound = $false
		Write-Host ("WARNING:  Expected file [" + $key + "] was not found in ZIP archive.")
		continue
	}
}

if ($bAllFilesFound)
{
	Write-Host ("All expected files found in ZIP archive.")
}


Write-Host ("Creating batch file to launch Sysinternals CMD console...")

$CMD_batchFile_outputPath = $progFilesDirectory_fullPath + "\Sysinternals_CMD_Console.bat"

"@ECHO OFF"                             | Out-File $CMD_batchFile_outputPath -Encoding "ASCII"
""                                      | Out-File $CMD_batchFile_outputPath -Encoding "ASCII" -Append
"SET PATH=%PATH%;$consoleTools_binPath" | Out-File $CMD_batchFile_outputPath -Encoding "ASCII" -Append
"SET PATH=%PATH%;$psTools_binPath"      | Out-File $CMD_batchFile_outputPath -Encoding "ASCII" -Append
""                                      | Out-File $CMD_batchFile_outputPath -Encoding "ASCII" -Append
"%ComSpec% /k"                          | Out-File $CMD_batchFile_outputPath -Encoding "ASCII" -Append
""                                      | Out-File $CMD_batchFile_outputPath -Encoding "ASCII" -Append

$CMD_batchFileShortcut_startMenuPath = $startMenuFolder_fullPath + "\Sysinternals Console" + "\Sysinternals CMD Console.LNK"

Write-Host ("Creating Start Menu shortcut for CMD console batch file...")
$CMD_batchFileShortCutObj            = $wshShellObj.CreateShortcut( $CMD_batchFileShortcut_startMenuPath )
$CMD_batchFileShortCutObj.TargetPath = $CMD_batchFile_outputPath
$CMD_batchFileShortCutObj.Save()


Write-Host ("Creating PowerShell script to launch Sysinternals PowerShell...")
$PS1_scriptFile_outputPath = $progFilesDirectory_fullPath + "\Sysinternals_PowerShell.ps1"

""                                         | Out-File $PS1_scriptFile_outputPath -Encoding "ASCII"
"`$env:Path += "";$consoleTools_binPath""" | Out-File $PS1_scriptFile_outputPath -Encoding "ASCII" -Append
"`$env:Path += "";$psTools_binPath"""      | Out-File $PS1_scriptFile_outputPath -Encoding "ASCII" -Append
""                                         | Out-File $PS1_scriptFile_outputPath -Encoding "ASCII" -Append

$PowerShell_ExePath = """%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe"""

$PS1_scriptFileShortcut_linkArguments = "-NoExit -ExecutionPolicy Bypass -File ""$PS1_scriptFile_outputPath"""

$PS1_scriptFileShortcut_startMenuPath = $startMenuFolder_fullPath + "\Sysinternals Console" + "\Sysinternals PowerShell.LNK"

Write-Host ("Creating Start Menu shortcut for PowerShell script file...")
$PS1_scriptFileShortCutObj            = $wshShellObj.CreateShortcut( $PS1_scriptFileShortcut_startMenuPath )
$PS1_scriptFileShortCutObj.TargetPath = $PowerShell_ExePath
$PS1_scriptFileShortCutObj.Arguments  = $PS1_scriptFileShortcut_linkArguments
$PS1_scriptFileShortCutObj.Save()


Write-Host ("All done!")

Pause ("Press any key to exit...")

