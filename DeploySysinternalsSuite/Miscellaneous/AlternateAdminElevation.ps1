
#$global:TRANSCRIPT = "C:\PowerShell_Logs\PSLOG_{0:MM-dd-yyyy}_$PID.txt" -f (Get-Date)
$transcriptFile = ("C:\PowerShell_Logs\PSLOG_{0:MM-dd-yyyy_HH.mm.ss}_$PID.txt" -f (Get-Date) )
Start-Transcript $transcriptFile

$scriptFullPath = $MyInvocation.MyCommand.Definition
# Set-PSBreakpoint -Script $scriptFullPath -Line 4

Write-Host "Press any key to continue..."
$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")




# The following Administrator-related code is shamelessly stolen from MSDN's "Virtual PC guy":
#
# Get the ID and security principal of the current user account:
#
$myWindowsID=[System.Security.Principal.WindowsIdentity]::GetCurrent()

$myWindowsPrincipal=new-object System.Security.Principal.WindowsPrincipal($myWindowsID)

# Get the security principal for the Administrator role:
#
$adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator

# Check to see if we are currently running "as Administrator":
#
if ($myWindowsPrincipal.IsInRole($adminRole))
{
	# We are running "as Administrator" - so change the title and background color to indicate this:
	#
	$Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Elevated)"

	$Host.UI.RawUI.BackgroundColor = "DarkBlue"

	clear-host
}
else {
	# We are not running "as Administrator" - so relaunch as administrator...:

	# Create a new process object that starts PowerShell:
	#
	$newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";

	# Specify the current script path and name as a parameter:
	#
	$newProcess.Arguments = $myInvocation.MyCommand.Definition;

	# Indicate that the process should be elevated:
	#
	$newProcess.Verb = "runas";

	# Start the new process:
	#
	[System.Diagnostics.Process]::Start($newProcess);

	# Exit from the current, unelevated, process:
	#
	exit
}

Write-Host -NoNewLine "Press any key to continue..."

$null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
#
# End of Administrator verification.  Main script begins below.

