#
# DeployWindowsTools:  This script, developed for Windows computers, allows
# administrators to deploy all or some of a selection of the most common
# freely-available Windows troubleshooting tools (including the Sysinternals
# Suite), all at once.  Why?  Because it's convenient and awesome :-)
#
#
# COPYRIGHT:
#
# Copyright (C) April, 2018  JackOfAll_IT_Trades
# (JackOfAll.IT.Trades@gmail.com)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License (LGPL) as published by the
# Free Software Foundation, version 3 (LGPLv3).
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License, version
# 3 for more details.
#
# You should have received a copy of the GNU Lesser General Public License,
# ver. 3, along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

param(
    [switch]$dontDownload,
    [switch]$dontInstall)


Write-Host ("
DeployWindowsTools:  A deployment script that installs a selection of the most
common freely-available Windows utilities and diagnostic tools, including the
Sysinternals Suite, all at once.  Why?  Because it's convenient and awesome :-)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License (LGPL) as published by the
Free Software Foundation, version 3 (LGPLv3).  For more details, read the code.

")


$winToolsDirName = "Windows-Tools"


$Arch = (Get-Process -Id $PID).StartInfo.EnvironmentVariables["PROCESSOR_ARCHITECTURE"];
if ($Arch -eq 'x86') {
    Write-Host -Object 'Running 32-bit PowerShell';
}
elseif ($Arch -eq 'amd64') {
    Write-Host -Object 'Running 64-bit PowerShell';
}


# Before we begin, initialize the mappings that will define where we search for each put each file in the suite:
#
function InitializeInstallersMapping
{
	$installersMapping = New-Object System.Collections.Specialized.OrderedDictionary

	# Example:
	#
	#	$installersMapping["7-Zip"]		=	@{	"relativePath" = ".\7-Zip\";
    #												"amd64" = @{	"downloadURL" = "https://www.7-zip.org/a/7z1801-x64.msi";
	#																"installFile" = "7z1801-x64.msi";
	#																"installArgs" = '/q INSTALLDIR="C:\\Program Files\\7-Zip';
	#															};
	#												"x86"   = @{	"downloadURL" = "https://www.7-zip.org/a/7z1801.msi";
	#																"installFile" = "7z1801.msi";
	#																"installArgs" = '/q INSTALLDIR="C:\\Program Files\\7-Zip';
	#															};
	#											};
	#

	$installersMapping["01 - Notepad++"]		=	@{	"relativePath" = ".\Notepad++\";
    												"amd64" = @{	"downloadURL" = "https://notepad-plus-plus.org/repository/7.x/7.5.6/npp.7.5.6.Installer.exe";
																	"installFile" = "npp.7.5.6.Installer.exe";
																	"installArgs" = "/S /D=""$ENV:ProgramFiles\\Notepad++"" ";
																};
													"x86"   = @{	"downloadURL" = "https://notepad-plus-plus.org/repository/7.x/7.5.6/npp.7.5.6.Installer.x64.exe";
																	"installFile" = "npp.7.5.6.Installer.x64.exe";
																	"installArgs" = "/S /D=""$ENV:ProgramFiles\\Notepad++"" ";
																};
												};

	$installersMapping["02 - 7-Zip"]			=	@{	"relativePath" = ".\7-Zip\";
    												"amd64" = @{	"downloadURL" = "https://www.7-zip.org/a/7z1801-x64.msi";
																	"installFile" = "7z1801-x64.msi";
																	"installArgs" = "/q INSTALLDIR=""$ENV:ProgramFiles\\7-Zip"" ";
																};
													"x86"   = @{	"downloadURL" = "https://www.7-zip.org/a/7z1801.msi";
																	"installFile" = "7z1801.msi";
																	"installArgs" = "/q INSTALLDIR=""$ENV:ProgramFiles\\7-Zip"" ";
																};
												};

	$installersMapping["03 - WinMerge"]			=	@{	"relativePath" = ".\WinMerge\";
													"all"   = @{	"downloadURL" = "http://downloads.sourceforge.net/winmerge/WinMerge-2.14.0-Setup.exe";
																	"installFile" = "WinMerge-2.14.0-Setup.exe";
																	"installArgs" = '/VERYSILENT /SP- /NORESTART';
																};
												};

	$installersMapping["04 - WinDirStat"]		=	@{	"relativePath" = ".\WinDirStat\";
													"all"   = @{	"downloadURL" = "https://www.fosshub.com/WinDirStat.html/windirstat1_1_2_setup.exe";
																	"installFile" = "windirstat1_1_2_setup.exe";
																	"installArgs" = '/S';
																};
												};

	$installersMapping["05 - WinPcap"]			=	@{	"relativePath" = ".\WinPcap\";
    												"all"   = @{	"downloadURL" = "https://www.winpcap.org/install/bin/WinPcap_4_1_3.exe";
																	"installFile" = "WinPcap_4_1_3.exe";
																	"installArgs" = '';
																	"Wait_For_It" = $TRUE;
																};
												};

	$installersMapping["06 - Wireshark"]		=	@{	"relativePath" = ".\Wireshark\";
    												"amd64" = @{	"downloadURL" = "https://1.na.dl.wireshark.org/win64/Wireshark-win64-2.6.3.exe";
																	"installFile" = "Wireshark-win64-2.6.3.exe";
																	"installArgs" = '/S';
																};
													"x86"   = @{	"downloadURL" = "https://1.na.dl.wireshark.org/win32/Wireshark-win32-2.6.3.exe";
																	"installFile" = "Wireshark-win32-2.6.3.exe";
																	"installArgs" = '/S';
																};
												};

	$installersMapping["07 - Sumatra PDF"]		=	@{	"relativePath" = ".\Sumatra PDF\";
    												"amd64" = @{	"downloadURL" = "https://www.sumatrapdfreader.org/dl/SumatraPDF-3.1.2-64-install.exe";
																	"installFile" = "SumatraPDF-3.1.2-64-install.exe";
																	"installArgs" = '/S';
																};
													"x86"   = @{	"downloadURL" = "https://www.sumatrapdfreader.org/dl/SumatraPDF-3.1.2-install.exe";
																	"installFile" = "SumatraPDF-3.1.2-install.exe";
																	"installArgs" = '/S';
																};
												};

	$installersMapping["08 - VLC media player"]	=	@{	"relativePath" = ".\VLC\";
    												"amd64" = @{	"downloadURL" = "https://get.videolan.org/vlc/3.0.4/win64/vlc-3.0.4-win64.exe";
																	"installFile" = "vlc-3.0.4-win64.exe";
																	"installArgs" = '/L=1033 /S';
																};
													"x86"   = @{	"downloadURL" = "https://get.videolan.org/vlc/3.0.4/win32/vlc-3.0.4-win32.exe";
																	"installFile" = "vlc-3.0.4-win32.exe";
																	"installArgs" = '/L=1033 /S';
																};
												};

	$installersMapping["09 - Sysinternals ZIP"]	=	@{	"relativePath" = ".\Sysinternals\";
    												"all"   = @{	"downloadURL" = "https://download.sysinternals.com/files/SysinternalsSuite.zip";
																	"installFile" = "SysinternalsSuite.zip";
																	"installArgs" = $null;
																};
												};

	$installersMapping["10 - Sysint. setup"]	=	@{	"relativePath" = ".\Sysinternals\";
    												"all"   = @{	"downloadURL" = "https://bitbucket.org/JackOfAll_IT_Trades/deployment_tools/raw/b756bfb29cd291a54df101c86d074a04009aae36/DeploySysinternalsSuite/DeploySysinternalsSuite.ps1";
																	"installFile" = "DeploySysinternalsSuite.ps1";
																	"installArgs" = '';
																	"Wait_For_It" = $TRUE;
																};
												};

	return $installersMapping
}


# The following Administrator-related code is shamelessly stolen from Keith Hill's awesome example 
# at "http://rkeithhill.wordpress.com/2013/04/05/powershell-script-that-relaunches-as-admin/":
#

function IsAdministrator
{
	$Identity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
	$Principal = New-Object System.Security.Principal.WindowsPrincipal($Identity)
	$Principal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
}


function IsUacEnabled
{
	(Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System).EnableLua -ne 0
}


#
# Administrator verification section (prelude to main script) :
#

if (!(IsAdministrator))
#if ((IsAdministrator))
{
	if (IsUacEnabled)
	{
		[string[]]$argList = @('-NoProfile', '-NoExit', '-File', '"' + $MyInvocation.MyCommand.Path + '"')
		$argList += $MyInvocation.BoundParameters.GetEnumerator() | Foreach {"-$($_.Key)", "$($_.Value)"}
		$argList += $MyInvocation.UnboundArguments
		Start-Process PowerShell.exe -Verb Runas -WorkingDirectory $pwd -ArgumentList $argList 
		return
	}
	else
	{
		$errorMessage = "ERROR:  You must be administrator to run this script!"
		Write-Host $errorMessage
		Write-Host "Press any key to continue..."
		$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
		throw $errorMessage
	}
}

#
# End of Administrator verification.  Main script begins below.


$dontDownload_onlyInstall = $dontDownload
$dontInstall_onlyDownload = $dontInstall

$windowsToolsFileMapping  = InitializeInstallersMapping


# First, compute the path to our script, and check for an Internet connection:
#
Write-Host ("Looking for path to DeployWindowsTools script...")
$scriptPath  = split-path -parent $MyInvocation.MyCommand.Definition

# Next, compute the temporary directory (".\Windows-Tools") where we'll store
# the installers (and eventually run them), and create it if necessary:
#
Write-Host ("Computing destination directory ('$winToolsDirName' folder)...")
$winToolsDir_fullPath = [io.path]::Combine( [io.path]::GetFullPath($scriptPath), $winToolsDirName)

if (!(Test-Path -Path $winToolsDir_fullPath -PathType Container)) {
	Write-Host ("Windows tools directory [" + $winToolsDir_fullPath + "] does not exist.  Creating...")
	New-Item -ItemType directory -Path $winToolsDir_fullPath > $null
}



if (!$dontDownload_onlyInstall) {
    #
    # Then, check for an Internet connection:
    #
    Write-Host ("Checking for Internet connection...")
    $internetConnected = (Test-Connection -computer www.google.com -count 1 -quiet)

    if ($internetConnected) {
	    Write-Host ("Internet connection present -- will attempt to download tools...")

	    $sortedKeys = $windowsToolsFileMapping.Keys
	    foreach ($item in $sortedKeys)
	    {
		    Write-Host ("Found application: [" + $item + "]...")

		    $currentInstaller_Dictionary     = $windowsToolsFileMapping[$item]
		    $installerDirectory_RelativePath = $currentInstaller_Dictionary["relativePath"]

		    $installerDirectory_FullPath     = [io.path]::GetFullPath($winToolsDir_fullPath + $installerDirectory_RelativePath)

		    Write-Host ("Checking for folder <'$installerDirectory_FullPath'>...")
		    $directoryExists = Test-Path -Path $installerDirectory_FullPath -PathType Container
            if (!($directoryExists)) {
	            Write-Host ("Subdirectory [" + $installerDirectory_FullPath + "] does not exist.  Creating...")
	            New-Item -ItemType directory -Path $installerDirectory_FullPath > $null
            }

		    # Check for "all" platform installer file, or specific "x86" / "amd64" versions:
		    #
		    $currentPlatform_Dictionary = $null
		    if ($currentInstaller_Dictionary.ContainsKey("all"))
		    {
			    $currentPlatform_Dictionary = $currentInstaller_Dictionary["all"]
		    }
		    elseif ($currentInstaller_Dictionary.ContainsKey($Arch))
		    {
			    $currentPlatform_Dictionary = $currentInstaller_Dictionary[$Arch]
		    }
		    else
		    {
			    throw "Item dictionary for <$item> does not contain any installer for platform <$Arch>!\n(Please check '$$installersMapping' dictionary for <$item>.)"
		    }

		    $installerFile_Name     = $currentPlatform_Dictionary["installFile"]
		    $installerFile_FullPath = [io.path]::Combine([io.path]::GetFullPath($installerDirectory_FullPath), $installerFile_Name)
		    $installerFile_Exists   = Test-Path -Path $installerFile_FullPath -PathType Leaf

		    if ($installerFile_Exists)
		    {
			    Write-Host ("Item <$item> already has an installer file ($installerFile_Name).  Skipping download attempt...")
			    continue
		    }

		    # Ok, if we get here, we know that our installer file is *not* present.
		    # Attempt to download that file:
		    #
		    $installerFile_URL  = $currentPlatform_Dictionary["downloadURL"]
		    $fileDownload_start = Get-Date
    #		$webClient          = New-Object System.Net.WebClient
    #       $webClient.Headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134';
		    Write-Host ("Attempting to download <$installerFile_URL>...")
    #		$webClient.DownloadFile($installerFile_URL, $installerFile_FullPath)
            [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
            Invoke-WebRequest -Uri $installerFile_URL -OutFile $installerFile_FullPath -UserAgent [Microsoft.PowerShell.Commands.PSUserAgent]::FireFox
            $elapsed = New-Timespan �Start $fileDownload_start �End (Get-Date)
		    Write-Host ("Completed!  [Elapsed time: $($elapsed.Seconds) second(s)]")

	    } # END foreach ($item in $sortedKeys) { ... }
    }
    else
    {
	    Write-Host ("Internet connection NOT present -- will look for installers in local directory <$winToolsDir_fullPath>...")
    }

} # END if (!$dontDownload_onlyInstall)  { ... }


Write-Host ("Verifying installers in <$winToolsDir_fullPath>...")

$sortedKeys = $windowsToolsFileMapping.Keys
foreach ($item in $sortedKeys)
{
	$currentInstaller_Dictionary     = $windowsToolsFileMapping[$item]
	$installerDirectory_RelativePath = $currentInstaller_Dictionary["relativePath"]

	$installerDirectory_FullPath     = [io.path]::GetFullPath($winToolsDir_fullPath + $installerDirectory_RelativePath)

	Write-Host ("Checking for installers in folder <'$installerDirectory_FullPath'>...")
	$directoryExists = Test-Path -Path $installerDirectory_FullPath -PathType Container

	# Check for "all" platform installer file, or specific "x86" / "amd64" versions:
	#
	$currentPlatform_Dictionary = $null
	if ($currentInstaller_Dictionary.ContainsKey("all"))
	{
		$currentPlatform_Dictionary = $currentInstaller_Dictionary["all"]
	}
	elseif ($currentInstaller_Dictionary.ContainsKey($Arch))
	{
		$currentPlatform_Dictionary = $currentInstaller_Dictionary[$Arch]
	}
	else
	{
		throw "Item dictionary for <$item> does not contain any installer for platform <$Arch>!\n(Please check '$$installersMapping' dictionary for <$item>.)"
	}

	$installerFile_Name     = $currentPlatform_Dictionary["installFile"]
	$installerFile_FullPath = [io.path]::GetFullPath($installerDirectory_FullPath + $installerFile_Name)
	$installerFile_Exists   = Test-Path -Path $installerFile_FullPath -PathType Leaf

	if (!$installerFile_Exists)
	{
		throw "Error:  No installer file found for item <$item>!  (Looking for <$installerFile_FullPath>...)";
	}

}

Write-Host ("All installers present.  (Yay!)  Continuing...")
	

if (!$dontInstall_onlyDownload)
{
	$sortedKeys = $windowsToolsFileMapping.Keys
	foreach ($item in $sortedKeys)
	{
		$currentInstaller_Dictionary     = $windowsToolsFileMapping[$item]
		$installerDirectory_RelativePath = $currentInstaller_Dictionary["relativePath"]

		$installerDirectory_FullPath = [io.path]::GetFullPath($winToolsDir_fullPath + $installerDirectory_RelativePath)

		$currentPlatform_Dictionary = $null
		if ($currentInstaller_Dictionary.ContainsKey("all"))
		{
			$currentPlatform_Dictionary = $currentInstaller_Dictionary["all"]
		}
		elseif ($currentInstaller_Dictionary.ContainsKey($Arch))
		{
			$currentPlatform_Dictionary = $currentInstaller_Dictionary[$Arch]
		}

		$installerFile_Name = $currentPlatform_Dictionary["installFile"]
		$installerFile_Args = $currentPlatform_Dictionary["installArgs"]

		$installerFile_FullPath = [io.path]::GetFullPath($installerDirectory_FullPath + $installerFile_Name)

        if ($installerFile_FullPath.EndsWith(".ps1"))
        {
            $installerFile_Args     = "-File " + $installerFile_FullPath + $installerFile_Args
            $installerFile_FullPath = "powershell.exe"
        }

		Write-Host ("Running installer <'$installerFile_FullPath'>...")


        if ($installerFile_Args -ne $null) {

		    if ( ($currentPlatform_Dictionary.ContainsKey("Wait_For_It")) -And
			     ($currentPlatform_Dictionary["Wait_For_It"] -eq $TRUE) )
		    {
                if ($installerFile_Args.Length -gt 0)
                {
			        Start-Process -Wait -FilePath `"$installerFile_FullPath`" -ArgumentList "$installerFile_Args"
                }
                else
                {
			        Start-Process -Wait -FilePath `"$installerFile_FullPath`"
                }
		    }
            else
            {
                if ($installerFile_Args.Length -gt 0)
                {
			        Start-Process -FilePath `"$installerFile_FullPath`" -ArgumentList "$installerFile_Args"
                }
                else
                {
			        Start-Process -FilePath `"$installerFile_FullPath`"
                }
            }

        } # END if ($installerFile_Args -ne $null) { ... }

	} # END foreach ($item in $sortedKeys) { ... }

} # END if (!$dontInstall_onlyDownload) { ... }



Write-Host ("All done!")

